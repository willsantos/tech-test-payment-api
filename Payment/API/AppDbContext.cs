﻿using API.Entities;
using Microsoft.EntityFrameworkCore;

namespace Repository
{
    public class AppDbContext : DbContext
    {
        public AppDbContext()
        {

        }

        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {

        }

        public DbSet<Sale> Sales { get; set; }
        public DbSet<Seller> Sellers { get; set; }
        public DbSet<Item> Items { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Item>().HasQueryFilter(x => x.Deleted == null);
            modelBuilder.Entity<Sale>().HasQueryFilter(x => x.Deleted == null);
            modelBuilder.Entity<Seller>().HasQueryFilter(x => x.Deleted == null);
        }

    }
}