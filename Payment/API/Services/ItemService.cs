﻿using API.Entities;
using API.Repositories;

namespace API.Services
{
    public class ItemService
    {
        private readonly ItemRepository _repository;

        public ItemService(ItemRepository repository)
        {
            _repository = repository;
        }

        public async Task<List<Item>> GetItemsAsync()
        {
            return await _repository.GetItemsAsync();
        }

        public async Task<Item> GetItemByIdAsync(int id)
        {
           
            var entity = await _repository.GetItemByIdAsync(id);

            if (entity == null)
            {
                throw new Exception("Item not found");
            }

            return entity;

        }

        public async Task<Item> GetItemByNameAsync(string name)
        {

            var entity = await _repository.GetItemByNameAsync(name);

            if (entity == null)
            {
                throw new Exception("Item not found");
            }

            return entity;

        }

        public async Task CreateItemAsync(Item item)
        {
            item.Created = DateTime.Now;
            await _repository.CreateItemAsync(item);
        }

        public async Task UpdateItemAsync(int id, Item item)
        {
            var entity = await _repository.GetItemByIdAsync(id);

            if (entity == null)
                throw new Exception("Item not found");

            
            entity.Modified = DateTime.Now;
            entity.Name = item.Name;
            entity.Price = item.Price;
            

            await _repository.UpdateItemAsync(entity);
        }

        public async Task DeleteItemAsync(int id)
        {
            var entity = await _repository.GetItemByIdAsync(id);

            if (entity == null)
            {
                throw new Exception("Item not found");
            }

            entity.Deleted = DateTime.Now;
            await _repository.DeleteItemAsync(entity);
        }

    }
}
