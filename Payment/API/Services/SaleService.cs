﻿using API.Entities;
using API.Enums;
using API.Repositories;

namespace API.Services
{
    public class SaleService
    {
        private readonly SaleRepository _repository;

        public SaleService(SaleRepository repository)
        {
            _repository = repository;
        }

        public async Task<List<Sale>> GetSalesAsync()
        {
            return await _repository.GetSaleAsync();
        }

        public async Task<Sale> GetSaleByIdAsync(int id)
        {

            var entity = await _repository.GetSaleByIdAsync(id);

            if (entity == null)
            {
                throw new Exception("Sale not found");
            }

            return entity;

        }


        public async Task CreateSaleAsync(Sale sale)
        {
            sale.Created = DateTime.Now;
            await _repository.CreateSaleAsync(sale);
        }

        public async Task UpdateSaleAsync(int id, StatusEnum status)
        {
            var entity = await _repository.GetSaleByIdAsync(id);


            if (entity == null)
                throw new Exception("Sale not found");

            entity.Modified = DateTime.Now;
            
            entity.Status = status;

            await _repository.UpdateSaleAsync(entity);
        }

        public async Task DeleteSaleAsync(int id)
        {
            var entity = await _repository.GetSaleByIdAsync(id);

            if (entity == null)
                throw new Exception("Sale not found");
            entity.Deleted = DateTime.Now;
            await _repository.DeleteSaleAsync(entity);
        }
    }
}
