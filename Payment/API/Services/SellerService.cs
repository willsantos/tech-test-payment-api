﻿using API.Entities;
using API.Repositories;

namespace API.Services
{
    public class SellerService
    {
        private readonly SellerRepository _repository;

        public SellerService(SellerRepository repository)
        {
            _repository = repository;
        }

        public async Task<List<Seller>> GetSellersAsync()
        {
            return await _repository.GetSellersAsync();
        }

        public async Task<Seller> GetSellerByIdAsync(int id)
        {

            var entity = await _repository.GetSellerByIdAsync(id);

            if (entity == null)
            {
                throw new Exception("Seller not found");
            }

            return entity;

        }

        public async Task<Seller> GetSellerByNameAsync(string name)
        {

            var entity = await _repository.GetSellerByNameAsync(name);

            if (entity == null)
            {
                throw new Exception("Seller not found");
            }

            return entity;

        }

        public async Task CreateSellerAsync(Seller seller)
        {
            seller.Created = DateTime.Now;
            await _repository.CreateSellerAsync(seller);
        }

        public async Task UpdateSellerAsync(int id, Seller seller)
        {
            var entity = await _repository.GetSellerByIdAsync(id);

            
            if (entity == null)
                throw new Exception("Seller not found");


            entity.Modified = DateTime.Now;
            entity.Name = seller.Name;
            entity.Cpf = seller.Cpf;
            entity.Email = seller.Email;
            entity.Phone = seller.Phone;
            

            await _repository.UpdateSellerAsync(entity);
        }

        public async Task DeleteSellerAsync(int id)
        {
            var entity = await _repository.GetSellerByIdAsync(id);

            if (entity == null)
                throw new Exception("Seller not found");

            entity.Deleted = DateTime.Now;

            await _repository.DeleteSellerAsync(entity);
        }
    }
}
