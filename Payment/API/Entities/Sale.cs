﻿using API.Enums;

namespace API.Entities
{
    public class Sale : BaseEntity
    {
        public string RequestNumber { get; set; }
        public DateTime RequestDate { get; set; }
        public StatusEnum Status { get; set; }
        public int SellerId { get; set; }
        public virtual Seller Seller { get; set; }
        public virtual List<Item> Items { get; set; }

    }
}
