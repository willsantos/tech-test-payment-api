﻿using API.Entities;
using Microsoft.EntityFrameworkCore;
using Repository;

namespace API.Repositories
{
    public class ItemRepository
    {
        private readonly AppDbContext _context;

        public ItemRepository(AppDbContext context)
        {
            _context = context;
        }

        public async Task<Item> GetItemByIdAsync(int id)
        {
            return await _context.Items.FindAsync(id);
        }

        public async Task<Item> GetItemByNameAsync(string name)
        {
            return await _context.Items.FirstOrDefaultAsync(x => x.Name == name);
        }

        public async Task<List<Item>> GetItemsAsync()
        {
            return await _context.Items.ToListAsync();
        }

        public async Task CreateItemAsync(Item item)
        {
            _context.Items.Add(item);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateItemAsync(Item item)
        {
            _context.Items.Update(item);
            await _context.SaveChangesAsync();
          
        }

        public async Task DeleteItemAsync(Item item)
        {
            _context.Items.Update(item);
            await _context.SaveChangesAsync();
          
        }

        
    }
}
