﻿using API.Entities;
using Microsoft.EntityFrameworkCore;
using Repository;

namespace API.Repositories
{
    public class SellerRepository
    {

        private readonly AppDbContext _context;

        public SellerRepository(AppDbContext context)
        {
            _context = context;
        }

        public async Task<Seller> GetSellerByIdAsync(int id)
        {
            return await _context.Sellers.FindAsync(id);
        }

        public async Task<Seller> GetSellerByNameAsync(string name)
        {
            return await _context.Sellers.FirstOrDefaultAsync(x => x.Name == name);
        }

        public async Task<List<Seller>> GetSellersAsync()
        {
            return await _context.Sellers.ToListAsync();
        }

        public async Task CreateSellerAsync(Seller seller)
        {
            _context.Sellers.Add(seller);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateSellerAsync(Seller seller)
        {
            _context.Sellers.Update(seller);
            await _context.SaveChangesAsync();

        }

        public async Task DeleteSellerAsync(Seller seller)
        {
            _context.Sellers.Update(seller);
            await _context.SaveChangesAsync();

        }

    }
}
