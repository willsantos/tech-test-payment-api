﻿namespace API.Enums
{
    public enum StatusEnum
    {
        Waiting,
        Approved,
        Canceled,
        Sent,
        Delivered
    }
}
